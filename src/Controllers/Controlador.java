
package Controllers;

import vista.dlgVenta;
import vista.dlgRegistros;

import Models.ventas;
import Models.dbProducto;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


 
public class Controlador implements ActionListener{
    
    private dlgVenta ve;
    private dlgRegistros reg;
    private ventas venta;
    private dbProducto dbp;
    private boolean A = false;

    public Controlador(dlgVenta ve, dlgRegistros reg, ventas venta, dbProducto dbp) {
        this.ve = ve;
        this.reg = reg;
        this.venta = venta;
        this.dbp = dbp;
        
        ve.btnRealizar.addActionListener(this);
        ve.btnGuardar.addActionListener(this);
        ve.btnBuscar.addActionListener(this);
        ve.btnNuevo.addActionListener(this);
        
    }
    
    public void limpiar(){
        ve.txtcodigo.setText("");
        ve.txtcantidad.setText("");
        ve.cmbTipos.setSelectedIndex(0);
        ve.txtcodigo.setText("");
        ve.txtcodigo.setText("");
    }
    
    public void iniciarVenta() {
        limpiar();
        ve.setTitle(":: Registro de ventas de Gasolina ::");
        ve.setSize(600, 600);
        ve.setLocationRelativeTo(null);
        ve.setVisible(true);
    }

    public void iniciarRegistro() {
        limpiar();
        reg.setTitle(":: Registro de ventas realizadas ::");
        reg.setSize(600, 600);
        reg.setLocationRelativeTo(null);
        reg.setVisible(true);
    }
    
    private void cerrarVenta() {
        ve.setVisible(false);
    }
    
    private void cerrarRegistro() {
        reg.setVisible(false);
    }

    private void cargarDatos() {
        DefaultTableModel modelo = new DefaultTableModel();
        ArrayList<ventas> lista = new ArrayList<>();
        try {
            lista = dbp.listar();
        } catch (Exception ex) {
        }
        
        modelo.addColumn("codigo");
        modelo.addColumn("cantidad");
        modelo.addColumn("tipo");
        modelo.addColumn("precio");
        modelo.addColumn("total");
        for (ventas producto : lista) {
            modelo.addRow(new Object[]{producto.getId(), producto.getCodigo(), producto.getCantidad(), producto.getTipo(), producto.getPrecio(), producto.getTotal()});
        }
        reg.jtrRegistro.setModel(modelo);
    }
    

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == ve.btnNuevo) {
            ve.txtcodigo.setEnabled(true);
            ve.txtcantidad.setEnabled(true);
            ve.cmbTipos.setEnabled(true);
            ve.btnRealizar.setEnabled(true);
            ve.btnGuardar.setEnabled(true);
            ve.btnBuscar.setEnabled(true);
        }
        if (e.getSource() == ve.btnRealizar) {
            int op = ve.cmbTipos.getSelectedIndex();
            switch (op) {
                case 1:
                    venta.setTipo(1);
                    venta.setPrecio(24.50);
                    break;
                case 2:
                    venta.setTipo(2);
                    venta.setPrecio(20.50);
                    break;
                default:
                    JOptionPane.showMessageDialog(ve, "Seleccione un Tipo de Gasolina");
                    return;
            }

            try {
                if (venta.calcularTotal() == -1.0) {
                    JOptionPane.showMessageDialog(ve, "No hay gasolina suficiente");
                    return;
                }
                venta.setCodigo(ve.txtcodigo.getText());
                venta.setCantidad(Integer.parseInt(ve.txtcantidad.getText()));
                
                ve.lblCantidad.setText(Integer.toString(venta.getCantidad()));
                ve.lblPrecio.setText(Double.toString(venta.getPrecio()));
                ve.lblTotal.setText(Double.toString(venta.calcularTotal()));
                venta.setTotal(Double.parseDouble(ve.lblTotal.getText()));
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(ve, "No se pudo realizar la venta, surgio el siguiente error: " + ex.getMessage());
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(ve, "No se pudo realizar la venta, surgio el siguiente error: " + ex2.getMessage());
            }

        }
        if (e.getSource() == ve.btnGuardar) {
            try {//isExiste
                if (!dbp.isExiste(venta.getCodigo())) {
                    dbp.insertar(venta);
                    limpiar();
                    JOptionPane.showMessageDialog(ve, "Se Guardo con exito");
                    A=false;
                    return;
                } 
                if(A==true){
                    try {
                        dbp.actualizar(venta);  
                        A=false;
                    } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(ve, "Fallo al actualizar, surgio el siguiente error: " + ex.getMessage());
                    } catch (Exception ex2) {
                        JOptionPane.showMessageDialog(ve, "Fallo al actualizar, surgio el siguiente error: " + ex2.getMessage());
                    }   
                }
                limpiar();
                JOptionPane.showMessageDialog(ve, "El codigo ya existe");
                return;

            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(ve, "Fallo al guardar, surgio el siguiente error: " + ex.getMessage());
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(ve, "Fallo al guardar, surgio el siguiente error: " + ex2.getMessage());
            }
        }
        if (e.getSource() == ve.btnBuscar) {
            try {
                venta = (ventas) dbp.buscar(ve.txtcodigo.getText());
                venta.setCodigo(ve.txtcodigo.getText());
                venta = venta;
                if (!dbp.buscar(venta.getCodigo()).equals(-1)) {
                    limpiar();
                    ve.txtcodigo.setText(venta.getCodigo());
                    ve.txtcantidad.setText(Integer.toString(venta.getCantidad()));
                    ve.cmbTipos.setSelectedIndex(venta.getTipo());
                    ve.lblCantidad.setText(Integer.toString(venta.getCantidad()));
                    ve.lblPrecio.setText(Double.toString(venta.getPrecio()));
                    ve.lblTotal.setText(Double.toString(venta.calcularTotal()));
                    this.A = true;
                } else {
                    JOptionPane.showMessageDialog(ve, "No se encontro ");
                }
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(ve, "No se pudo encontrar, surgio el siguiente error: " + ex.getMessage());
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(ve, "No se pudo encontrar, surgio el siguiente error: " + ex2.getMessage());
            }

        }
        if (e.getSource() == reg.jButton1) {
            cargarDatos();
        }
        if (e.getSource() == ve.btnMostrarTodo) {
            ve.setVisible(false);
            iniciarRegistro();
        }
        
    }

    public static void main(String[] args) {
        ventas venta = new ventas();
        dbProducto dbp = new dbProducto();
        dlgVenta ve = new dlgVenta(new JFrame(), true);
        dlgRegistros reg = new dlgRegistros(new JFrame(), true);
        Controlador con = new Controlador(ve, reg, venta ,dbp);
        
        con.iniciarVenta();
    }   
    
    
    
}